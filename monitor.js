var watch = require('node-watch');
var fs = require('fs');
var exec = require('child_process').execFile;
var file = 'C:/wamp64/www/cmd';
watch(file, function (evt, filename) {
    fs.readFile(file, 'utf8', function (err, data) {
        data = data.trim();
        if (data) {
            console.log("Executing", data);
            var parts = data.split('/');
            var prog = parts.pop()
            delete parts[parts.length];
            exec(prog, {
                cwd: parts.join('\\')
            });
            fs.writeFile(file, '');
        }
    });
});