<?php
    require 'core/config.php';
    require 'core/mysql.php';
    require 'core/game.php';
    $game = new Game();
    $mysql = new MySQL();
    $servers = $mysql->find("SELECT * FROM servers");
?>
<!DOCTYPE html>
<html lang="en">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<style>
    td {
        vertical-align: middle !important;
    }

    .badge-default {
        background-color: #ddd;
    }
    .badge-info {
        background-color: #f0ad4e;
    }
    .badge-success {
        background-color: #24dba2;
    }

    .glyphicon {
        vertical-align: middle;
        color: #24dba2;
    }

    a.glyphicon,a .glyphicon {
        vertical-align: middle !important;
        color: #fff;
    }
</style>
</head>
<body>
    <div class="container">
        <div class="col-md-6 col-md-offset-3">
            <table class="table">
                <thead>
                    <tr>
                        <th width="1"></th>
                        <th>Server</th>
                        <th class="text-center">Status</th>
                        <th class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($servers as $server) { ?>
                        <tr data-process="<?php echo $server['process']; ?>" data-appid="<?php echo $server['app_id']; ?>">
                            <td width="1"><i class="glyphicon glyphicon-tasks"></i></td>
                            <td><?php echo $server['name']; ?></td>
                            <td class="text-center"><span class="badge"></span></td>
                            <td class="actions text-right">
                                <a href="#" class="start btn btn-sm btn-success glyphicon glyphicon-off" data-toggle="tooltip" data-placement="top" title="Start" data-cmd="start" data-appid="<?php echo $server['app_id']; ?>" data-exe="<?php echo $server['executable']; ?>" data-args="<?php echo $server['args']; ?>"></a>
                                <a href="#" class="stop btn btn-sm btn-danger glyphicon glyphicon-off" data-toggle="tooltip" data-placement="top" title="Stop" data-cmd="stop" data-process="<?php echo $server['process']; ?>"></a>
                                <a href="#" class="btn btn-sm btn-warning glyphicon glyphicon-download-alt" data-toggle="tooltip" data-placement="top" title="Update" data-cmd="update" data-appid="<?php echo $server['app_id']; ?>"></a>
                                <a href="/edit.php?id=<?php echo $server['id']; ?>" class="btn btn-sm btn-info glyphicon glyphicon-edit"data-toggle="tooltip" data-placement="top" title="Edit" ></a>
                                <a href="/processor.php?cmd=delete&id=<?php echo $server['id']; ?>&appid=<?php echo $server['app_id']; ?>"data-toggle="tooltip" data-placement="top" title="Delete"  onclick="return confirm('Are you sure you want to delete this server?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <a href="#" class="new btn btn-primary"><i class="glyphicon glyphicon-plus"></i> New Server</a>
            <form action="/processor.php" class="horizontal-form hidden">
                <input type="hidden" name="cmd" value="install">
                <div class="form-group">
                    <label for="app_id">App ID</label>
                    <input type="number" name="app_id" class="form-control" id="app_id" placeholder="Steam App ID">
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Server Name">
                </div>
                <button type="submit" class="btn btn-success">Install</button>
            </form>
        </div>
    </div>
    <script src="//code.jquery.com/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip()
        $('.new').on('click', function (e) {
            e.preventDefault();
            if ($('form').hasClass('hidden')) {
                $('form').removeClass('hidden');
            } else {
                $('form').addClass('hidden');
            }
        });
        function check() {
            $('tbody tr').each(function () {
                var el = $(this);
                var data = el.data();
                $.getJSON('/processor.php?cmd=check&process=' + data.process + '&appid=' + data.appid, function (data) {
                    if (data.updating) {
                        $('.badge', el).removeClass('badge-default').removeClass('badge-success').addClass('badge-info').text('Updating');
                    } else {
                        if (data.process) {
                            $('.start', el).hide();
                            $('.stop', el).show();
                            $('.badge', el).removeClass('badge-default').addClass('badge-success').text('Running');
                        } else {
                            $('.start', el).show();
                            $('.stop', el).hide();
                            $('.badge', el).removeClass('badge-success').addClass('badge-default').text('Offline');
                        }
                    }
                });
            });
        }
        setInterval(check, 1000);
        check();
        $('.actions a').on('click', function (e) {
            var el = $(this);
            var data = el.data();
            if (el.attr('href') == '#') {
                e.preventDefault();
                delete data.placement;
                delete data.toggle;
                delete data['bs.tooltip'];
                $.getJSON('/processor.php?' + $.param(data));
            }
        })
    </script>
</body>
</html>
