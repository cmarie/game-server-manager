<?php
    
    class Process
    {
        private function processes()
        {
            $processes = [];
            $processList = win32_ps_list_procs();
            foreach($processList as $process) {
                $processes[] = [
                    'pid' => $process['pid'],
                    'exe' => $process['exe'],
                    'mem' => $process['mem']['working_set_size'] * 0.000125,
                ];
            }
            return $processes;
        }
        
        public function check($cmd = null)
        {
            if($cmd) {
                $processes = $this->processes();
                foreach($processes as $process) {
                    if(stristr($process['exe'], $cmd)) {
                        return $process;
                    }
                }
            }
            
            return false;
        }
        
        public function start($cmd = null)
        {
            file_put_contents('cmd', $cmd);
            
            return $cmd;
        }
        
        public function stop($exe)
        {
            $process = $this->check($exe);
            $handle = popen("taskkill /f /PID " . $process['pid'], "r");
            pclose($handle);
        }
    }
    