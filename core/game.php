<?php
    require 'process.php';
    
    class Game extends Process
    {
        public function install($appid)
        {
            $cmd = "C:/steamCMD/steamcmd.exe +login anonymous +force_install_dir C:/steamCMD/servers/" . $appid . " +app_update " . $appid . " validate +quit";
            $handle = popen($cmd, "r");
            pclose($handle);
        }
        
        public function update($appid)
        {
            $cmd = "C:/steamCMD/steamcmd.exe +login anonymous +force_install_dir C:/steamCMD/servers/" . $appid . " +app_update " . $appid . " +quit";
            $handle = popen($cmd, "r");
            pclose($handle);
        }
        
        public function launch($cmd)
        {
            $this->start($cmd);
        }
        
        public function close($exe)
        {
            $task = $this->check($exe);
            if($task) {
                $this->stop($task['pid']);
            }
        }
    }
    