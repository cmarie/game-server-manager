<?php
    
    class MySQL
    {
        private $_conn;
        
        public function __construct()
        {
            $this->_conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE);
            if($this->_conn->connect_error) {
                die("Connection failed: " . $this->_conn->connect_error);
            }
        }
        
        public function find($sql)
        {
            return $this->_conn->query($sql);
        }
        
        public function read($table, $id)
        {
            $sql = sprintf("SELECT * FROM `%s` WHERE `id` = %s", $table, $id);
            
            return $this->_conn->query($sql)->fetch_assoc();
        }
        
        public function query($sql)
        {
            return $this->_conn->query($sql) === true;
        }
    }