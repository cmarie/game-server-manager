<?php
    require 'core/config.php';
    require 'core/mysql.php';
    require 'core/game.php';
    $game = new Game();
    $mysql = new MySQL();
    header('content-type: application/json');
    $params = $_GET ? $_GET : [];
    switch($params['cmd']) {
        case "install":
            $sql = sprintf("INSERT INTO `servers` (`name`, `app_id`) VALUES('%s', %s)", addslashes($params['name']), $params['app_id']);
            if($mysql->query($sql)) {
                $game->install($params['app_id']);
            }
            header('Location: /');
            break;
        case "update":
            $game->update($params['appid']);
            break;
        case "start":
            $cmd = "C:/steamCMD/servers/" . $params['appid'] . '/' . $params['exe'] . ' ' . $params['args'];
            echo $game->start($cmd);
            echo json_encode(['code' => 200]);
            break;
        case "stop":
            $game->stop($params['process']);
            echo json_encode(['code' => 200]);
            break;
        case "edit":
            $sql = sprintf("UPDATE `servers` SET `name` = '%s', `executable` = '%s', `process` = '%s' WHERE `id` = %s", addslashes($params['name']), $params['executable'], $params['process'], $params['id']);
            if($mysql->query($sql)) {
                header('Location: /');
            }
            break;
        case "delete":
            $sql = sprintf("DELETE FROM `servers` WHERE `id` = '%s'", $params['id']);
            if($mysql->query($sql)) {
                exec(sprintf("rd /s /q %s", escapeshellarg("C:/steamCMD/servers/" . $params['appid'])));
                header('Location: /');
            }
            break;
        case "check":
            $files = @scandir("C:/steamCMD/servers/" . $params['appid'] . '/steamapps/downloading/' . $params['appid']);
            echo json_encode([
                'code' => 200,
                'process' => $game->check($params['process']),
                'updating' => count($files) > 2
            ]);
            break;
    }