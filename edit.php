<?php
    require 'core/config.php';
    require 'core/mysql.php';
    $mysql = new MySQL();
    $server = $mysql->read('servers', $_GET['id']);
?>
<!DOCTYPE html>
<html lang="en">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="col-md-6 col-md-offset-3">
            <h2>Editing <?php echo $server['name'];?></h2>
            <form action="/processor.php" class="horizontal-form">
                <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                <input type="hidden" name="cmd" value="edit">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Server Name" value="<?php echo $server['name']; ?>">
                </div>
                <div class="form-group">
                    <label for="executable">Executable</label>
                    <input type="text" name="executable" class="form-control" id="executable" placeholder="Executable Filename" value="<?php echo $server['executable']; ?>">
                </div>
                <div class="form-group">
                    <label for="process">Process</label>
                    <input type="text" name="process" class="form-control" id="process" placeholder="Process Filename" value="<?php echo $server['process']; ?>">
                </div>
                <button type="submit" class="btn btn-success">Save</button>
            </form>
        </div>
    </div>
    <script src="//code.jquery.com/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
